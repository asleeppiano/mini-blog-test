# mini-medium

[demo](https://sheltered-bayou-40324.herokuapp.com/)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Json server
```
npm run start-api
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
