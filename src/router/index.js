import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Login from "../views/Login.vue";
import Edit from "../views/Edit.vue";
import NewPost from "../views/NewPost.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/:page(\\d+)?",
    name: "Home",
    component: Home
  },
  {
    path: "/login",
    name: "Login",
    component: Login
  },
  {
    path: "/edit/:postid(\\d+)?",
    name: "Edit",
    component: Edit
  },
  {
    path: "/newpost",
    name: "NewPost",
    component: NewPost
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, _from, next) => {
  if (Vue.auth.isAuthenticated()) {
    if (
      (to.name !== "Home" && Vue.auth.role !== "writer") ||
      to.name === "Login"
    ) {
      next({ name: "Home" });
    } else {
      next();
    }
  } else {
    if (to.name === "Edit" || to.name === "NewPost") {
      next({ name: "Login" });
    } else {
      next();
    }
  }
});

export default router;
