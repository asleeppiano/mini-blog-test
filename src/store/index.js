import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    totalCount: 0,
    posts: [],
    user: ""
  },
  getters: {
    getFetchedPostById: state => id => {
      return state.posts.find(post => id === post.id);
    }
  },
  mutations: {
    setPosts(state, posts) {
      state.posts = posts;
    },
    setPostById(state, { id, post }) {
      const idx = state.posts.findIndex(post => post.id === id);
      Vue.set(state.posts, idx, post);
    },
    setUser(state, user) {
      state.user = user;
    },
    setTotalCount(state, totalCount) {
      state.totalCount = totalCount;
    }
  },
  actions: {
    getPosts({ commit }, page) {
      console.log("page", page);
      if (!page) {
        page = 1;
      }
      fetch(`${process.env.VUE_APP_API_ADDR}/posts?_page=${page}&_limit=10`)
        .then(res => {
          commit("setTotalCount", res.headers.get("X-Total-Count"));
          return res.json();
        })
        .then(res => {
          commit("setPosts", res);
        });
    }
  },
  modules: {}
});
