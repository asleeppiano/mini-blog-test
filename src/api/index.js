export async function updatePost(post, id) {
  return await fetch(`${process.env.VUE_APP_API_ADDR}/posts/${id}`, {
    method: "PATCH",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(post)
  });
}

export async function createNewPost(post) {
  return await fetch(`${process.env.VUE_APP_API_ADDR}/posts`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(post)
  });
}

export async function deletePost(id) {
  const res = await fetch(`${process.env.VUE_APP_API_ADDR}/posts/${id}`, {
    method: "DELETE"
  });
  if (res.ok) {
    return res;
  } else {
    throw new Error("deletion error");
  }
}

export async function getPost(id) {
  const res = await fetch(`${process.env.VUE_APP_API_ADDR}/posts?id=${id}`);
  const post = await res.json();
  if (res.ok) {
    return post[0];
  } else {
    throw new Error("get post error");
  }
}
