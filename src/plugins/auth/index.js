import Auth from "./auth.js";

function AuthPlugin(Vue) {
  const auth = new Auth();

  Object.defineProperty(Vue, "auth", {
    get() {
      return auth;
    }
  });

  Object.defineProperty(Vue.prototype, "$auth", {
    get() {
      return auth;
    }
  });
}

if (typeof window !== "undefined" && window.Vue) {
  window.Vue.use(AuthPlugin);
}

export default AuthPlugin;
