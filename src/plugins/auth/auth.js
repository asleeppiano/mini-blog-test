import { ERROR_MESSAGE } from "../../constants";

export default class Auth {
  login(email, password) {
    return fetch(`http://localhost:3000/users?login=${email}`)
      .then(res => res.json())
      .then(user => {
        console.log("user", user);
        if (user.length === 0) {
          throw new Error(ERROR_MESSAGE.EMAIL);
        }
        if (user[0].password === password) {
          localStorage.setItem("user", JSON.stringify(user[0]));
          return user[0];
        } else {
          throw new Error(ERROR_MESSAGE.PASSWORD);
        }
      });
  }

  logout() {
    localStorage.removeItem("user");
  }

  get role() {
    const user = JSON.parse(localStorage.getItem("user"));
    if (user) {
      return user.role;
    }
    return "";
  }

  isAuthenticated() {
    if (localStorage.getItem("user")) {
      console.log("isAuthenticated");
      return true;
    }
    return false;
  }
}
